﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cfservices
{

    public class MyData
    {
        public Covid data { get; set; }
        public DateTime dt { get; set; }
    }
    public class Covid
    {
        [JsonProperty("location")]
        public string location { get; set; }
        [JsonProperty("confirmed")]
        public int  confirmed { get; set; }
        [JsonProperty("deaths")]
        public int deaths { get; set; }
        [JsonProperty("recovered")]
        public int recovered { get; set; }
        public int active { get; set; }
    }

    public class NewData
    {
        public string CountryName { get; set; }
        public string IsoCountry { get; set; }
        public int confirmedCase { get; set; }
        public int deaths { get; set; }
        public int recovered { get; set; }
        public int active { get; set; }
        public string date { get; set; }

    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace cfservices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        // GET: api/<CountryController>
        [HttpGet]
        public IActionResult Get()
        {
            var url = $"http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON/debug?";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";
                using (WebResponse country = request.GetResponse())
                {
                    using (Stream list = country.GetResponseStream())
                    {
                        using (StreamReader countries = new StreamReader(list))
                        {
                            string listcountry = countries.ReadToEnd();
                            return Ok(listcountry);
                        }
                    }
                }
        }
    }
}

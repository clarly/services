﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


namespace cfservices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CovidController : ControllerBase
    {

        // GET api/<CovidController>/DO
        [HttpGet("{code}")]
        public IActionResult Get(string code)
        {
            var url = "https://covid2019-api.herokuapp.com/v2/country/"+ code;
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            using (WebResponse covid = request.GetResponse())
            {
                using (Stream obCov = covid.GetResponseStream())
                {
                    using (StreamReader coov = new StreamReader(obCov))
                    {
                      string cood19 = coov.ReadToEnd();
                       var cov = JsonConvert.DeserializeObject<MyData>(cood19);
                        NewData obj = new NewData
                        {
                            CountryName = cov.data.location,
                            IsoCountry = code,
                            confirmedCase = cov.data.confirmed,
                            deaths = cov.data.deaths,
                            recovered = cov.data.recovered,
                            active = cov.data.active,
                            date = cov.dt.ToString("yyyy-MM-dd")
                        };
                        return Ok(obj);
                    }
                }
            }
        }

    }
}
